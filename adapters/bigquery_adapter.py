import logging

import gcsfs
from google.cloud import bigquery


class BigQueryAdapter:
    def __init__(self, project_id):
        self.client = bigquery.Client(project=project_id)
        self.gcs_file_system = gcsfs.GCSFileSystem(project=project_id)

    def create_external_table(self, project, dataset, table_id, source_uri, schema):
        dataset_ref = bigquery.DatasetReference(project, dataset)
        table = bigquery.Table(dataset_ref.table(table_id), schema=schema)

        external_config = bigquery.ExternalConfig("CSV")
        external_config.source_uris = [
            source_uri
        ]
        external_config.options.skip_leading_rows = 1  # optionally skip header row
        table.external_data_configuration = external_config

        try:
            # Create a permanent table linked to the GCS file
            table = self.client.create_table(table)  # API request
        except Exception as err:
            logging.info(err)

    def load_table_from_json(self, client, project, dataset, table, content):
        jobconfig = bigquery.LoadJobConfig()
        jobconfig.write_disposition = "WRITE_TRUNCATE"
        response = client.load_table_from_json(json_rows=content, project=project,
                                               destination=f"{project}.{dataset}.{table}",
                                               job_config=jobconfig).result()
        return True

    def create_table(self, project, dataset, table_id, schema):
        dataset_ref = bigquery.DatasetReference(project, dataset)
        table = bigquery.Table(dataset_ref.table(table_id), schema=schema)
        try:
            table_created = self.client.create_table(table)  # API request
            return table_created
        except Exception as err:
            logging.info(err)
            return None

    def exec_query(self, query):
        response = self.client.query(query).result()
        return response
