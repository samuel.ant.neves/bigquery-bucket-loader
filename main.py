from adapters.bigquery_adapter import BigQueryAdapter
from services.hash_service import HashService
from services.schema_service import SchemaService

project_id = "gcptools-dev"
dataset = "teste_dataset"
table_id = "external_table"
map_table = "map_table"

source_uri = "gs://teste-load-bq/data.csv"
schema_uri = "gs://teste-load-bq/data.json"

hash_service = HashService()
bigquery_adapter = BigQueryAdapter(project_id)
schema_service = SchemaService(bigquery_adapter, project_id)

raw_schema = schema_service.get_raw_schema_from_gcs(schema_uri)
hash_schema = schema_service.generate_new_schema_and_map(raw_schema)
bigquery_schema = schema_service.generate_bigquery_schema(hash_schema, "hash")
external_table = bigquery_adapter.create_external_table(project_id, dataset, table_id, source_uri, bigquery_schema)
final_schema = schema_service.generate_final_table_bq_schema(raw_schema)

final_table = bigquery_adapter.create_table(project_id, dataset, f'final_{table_id}', final_schema)
generated_query = schema_service.generate_query(project_id, dataset, table_id, f'final_{table_id}', hash_schema)

response = bigquery_adapter.exec_query(generated_query)
print(response)
# w_states = list(query_job)  # Waits for query to finish
# print("There are {} states with names starting with W.".format(len(w_states)))
