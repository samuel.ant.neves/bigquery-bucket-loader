import hashlib


class HashService:
    @staticmethod
    def generate_hash(string):
        return str(f'a{hashlib.md5(string.encode("utf-8")).hexdigest()}')
