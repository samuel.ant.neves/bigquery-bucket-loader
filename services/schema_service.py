import json

import gcsfs
import jinja2
from google.cloud import bigquery
from jinjasql import JinjaSql

from services.hash_service import HashService

TEMPLATE = """
INSERT INTO {{final_table}}
 ( 
{% for row in default_fields %} 
    {{ row['name']}},
{% endfor %}
 tags 
)
SELECT
  {% for row in default_fields %}
   {{row['hash']}} as {{row['name']}},
  {% endfor %}
       [
       {% for row in nested_fields[:-1] %}
       struct('{{row['name']}}' as key, CAST({{row['hash']}} as STRING) as value),
       {% endfor %}
       struct('{{nested_fields[-1]['name']}}' as key, CAST({{nested_fields[-1]['hash']}} as STRING) as value)
        ] as tags
 FROM `{{ external_table }}`

 """


class SchemaService:
    def __init__(self, bigquery_adapter, project_id):
        self.gcs_file_system = gcsfs.GCSFileSystem(project=project_id)
        self.bigquery_adapter = bigquery_adapter
        self.jinja = JinjaSql()

    def generate_new_schema_and_map(self, schema):
        adapted_schema = []
        for schema_field in schema:
            hash = HashService().generate_hash(schema_field['name'])
            schema_field["hash"] = hash
            adapted_schema.append(schema_field)
        return adapted_schema

        # adapted_schema.append(bigquery.SchemaField(hash, col['type']))
        # map_hash.append({"key": hash, "value": col['name']})name

    def get_raw_schema_from_gcs(self, schema_uri):
        original_schema = []
        with self.gcs_file_system.open(schema_uri) as f:
            bigquery_columns = json.load(f)
            for col in bigquery_columns:
                original_schema.append(col)
        return original_schema

    def generate_bigquery_schema(self, schema, field):
        adapted_schema = []
        for schema_field in schema:
            adapted_schema.append(bigquery.SchemaField(schema_field[field], schema_field['type']))
        return adapted_schema

    def generate_final_table_bq_schema(self, schema):
        default_fields = list(filter(lambda x: x['category'] != "tags", schema))
        # nested_fields = list(filter(lambda x: x['category'] == "tags", schema))
        final_table_schema = []
        for field in default_fields:
            final_table_schema.append(bigquery.SchemaField(field['name'], field['type']))
        final_table_schema.append(bigquery.SchemaField("tags", mode="REPEATED", field_type="RECORD", fields=[
            bigquery.SchemaField("key", "STRING"),
            bigquery.SchemaField("value", "STRING")
        ]))
        return final_table_schema

    def generate_query(self, project, dataset, table, final_table, adapted_schema):

        default_fields = list(filter(lambda x: x['category'] != "tags", adapted_schema))

        nested_fields = list(filter(lambda x: x['category'] == "tags", adapted_schema))
        data = {
            "external_table": f'{project}.{dataset}.{table}',
            "nested_fields": nested_fields,
            "default_fields": default_fields,
            "final_table": f'{dataset}.{final_table}'
        }
        template = jinja2.Template(TEMPLATE)
        generated_query = template.render(data)
        return generated_query
